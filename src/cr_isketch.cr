require "kemal"
require "kilt/slang"

require "./cr_isketch/*"

# Matches GET "http://host:port/"
get "/" do
  render "./src/views/index.slang"
end

# Creates a WebSocket handler.
# Matches "ws://host:port/socket"
ws "/socket" do |socket|
  socket.on_message do |msg|
    puts SimpleMessage.from_json(msg).action
  end
end

Kemal.run
