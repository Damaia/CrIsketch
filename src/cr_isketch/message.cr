require "json"

class Message
  JSON.mapping(
    action: String,
    posX: Float64,
    posY: Float64,
    button: Int32,
    firstClick: Bool
  )
end

class SimpleMessage
  JSON.mapping(
    action: String
  )
end
