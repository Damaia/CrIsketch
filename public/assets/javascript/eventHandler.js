var socket = new WebSocket("ws://0.0.0.0:3000/socket");

$('#canvas').attr('width', parseInt($('#canvas').css('width')))
$('#canvas').attr('height', parseInt($('#canvas').css('height')))

/* Options */
// $('#nickChangeBox').submit(function() {
//   socket.send('nickChange', $('#nickChange').val())
//   $('#nickChange').val('')
//   return false
// })

// socket.on('nickOk', function(nick) {
//   $('#nickStatus').text('Nickname successfully changed to ' + nick + '!')
// })
// socket.on('nickKo', function(nick) {
//   $('#nickStatus').text('Nickname ' + nick + ' is already taken. Try something else!')
// })
//
// /* Guess chat */
// $('#guessBox').submit(function() {
//   socket.send('guess', $('#guessWrite').val())
//   $('#guessWrite').val('')
//   return false
// })

// socket.on('guess', function(msg) {
//   $('#messages').append($('<li>').text(msg))
// })

/* Normal chat */
// $('#chat').submit(function() {
//   socket.send('chat', $('#chatWrite').val())
//   $('#chatWrite').val('')
//   return false
// })

// socket.on('chat', function(msg) {
//   $('#chatMessages').append($('<li>').text(msg))
// })

/* Game state */
// socket.on('wordChange', function(word) {
//   $('#word').text(word)
// })
//
// socket.on('scoreUpdate', function(scores) {
//   $('#score').text('')
//   for (var i = 0; i < scores.length; i++) {
//     $('#score').append($('<li>').text(scores[i][0] + ': ' + scores[i][1]))
//   }
// })
//
// socket.on('endGame', function(win, player) {
//   // Update game status
//   if (win)
//     $('#messages').append($('<li>').text(player + ' guessed the word and won!'))
//   else
//     $('#messages').append($('<li>').text(player + ' disconnected. The drawer has changed and a new word has been picked.'))
//
//   $('#word').text('Someone is drawing...')
//
//   // Clear canvas
//   var c = document.getElementById('canvas')
//   var ctx = c.getContext('2d')
//   ctx.clearRect(0, 0, c.width, c.height);
// })

/* Drawing */
$('#canvas').on('mousedown', function(e) {
  var c = document.getElementById('canvas')
  var msg = {
    action: "drawing",
    posX: e.offsetX / c.width,
    posY: e.offsetY / c.height,
    button: e.button,
    firstClick: true
  }
  socket.send(JSON.stringify(msg))
})

$('#canvas').on('mousemove', function(e) {
  var c = document.getElementById('canvas')
  var msg = {
    action: "drawing",
    posX: e.offsetX / c.width,
    posY: e.offsetY / c.height,
    button: e.button,
    firstClick: false
  }
  socket.send(JSON.stringify(msg))
})

$('#canvas').on('mouseup', function(e) {
  socket.send(JSON.stringify({action: 'stop_drawing'}))
})

$('#canvas').mouseleave(function(e) {
  socket.send(JSON.stringify({action: 'stop_drawing'}))
})

// socket.on('drawing', function(x, y, btn, brushColor, brushSize, prevPos) {
//   // Get canvas and context
//   // BUG: writing doesn't write at the right place if window is resized
//   var c = document.getElementById('canvas')
//   var ctx = c.getContext('2d')
//
//   if (btn == 2) {
//     ctx.strokeStyle = '#FFE' // same as default background color of canvas
//     ctx.fillStyle = '#FFE'
//     ctx.lineWidth = brushSize * 10
//   }
//   else if (btn == 0) {
//     ctx.strokeStyle = brushColor
//     ctx.fillStyle = brushColor
//     ctx.lineWidth = brushSize * 5
//   }
//
//   // Draw line between previous point and current point
//   ctx.beginPath();
//   ctx.moveTo(prevPos[0] * c.width - brushSize / 2, prevPos[1] * c.height - brushSize / 2)
//   ctx.lineTo(x * c.width - brushSize / 2, y * c.height - brushSize / 2)
//   ctx.stroke()
// })
